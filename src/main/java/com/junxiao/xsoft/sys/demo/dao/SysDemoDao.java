package com.junxiao.xsoft.sys.demo.dao;

import com.junxiao.xsoft.base.dao.BaseDao;
import com.junxiao.xsoft.sys.demo.entity.SysDemo;

/**
 * crud测试dao
 * 
 * @copyright © 2016 大连骏骁网络科技有限公司
 * @author 程旭(cxmail@qq.com)
 * @createDate 2016-05-29
 * @version: V1.0.0
 */
public interface SysDemoDao extends BaseDao<SysDemo> {

}
