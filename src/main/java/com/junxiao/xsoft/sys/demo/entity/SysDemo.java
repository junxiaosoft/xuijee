package com.junxiao.xsoft.sys.demo.entity;

import java.util.Date;

import com.junxiao.xsoft.base.annotation.Key;

/**
 * crud测试实体
 * 
 * @copyright © 2016 大连骏骁网络科技有限公司
 * @author 程旭(cxmail@qq.com)
 * @createDate 2016-05-29
 * @version: V1.0.0
 */
public class SysDemo {

	// 主键
	@Key
	private String id;
	// 名称
	private String name;
	// 电话
	private String phone;
	// 头像
	private String img;
	// 邮箱
	private String email;
	// 生日
	private Date birthday;
	// 创建时间
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}