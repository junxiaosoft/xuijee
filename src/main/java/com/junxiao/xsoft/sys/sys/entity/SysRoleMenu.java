package com.junxiao.xsoft.sys.sys.entity;

import com.junxiao.xsoft.base.annotation.Key;

/**
 * 角色权限
 * 
 * @copyright © 2016 大连骏骁网络科技有限公司
 * @author 程旭(cxmail@qq.com)
 * @createDate 2016-1-22
 * @version: V1.0.0
 */
public class SysRoleMenu {

	// 角色id
	@Key
	private String roleId;
	// 菜单id
	@Key
	private String menuId;
	// 其他权限
	private String operate;

	public SysRoleMenu() {

	}

	public SysRoleMenu(String roleId, String menuId) {
		this.roleId = roleId;
		this.menuId = menuId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getOperate() {
		return operate;
	}

	public void setOperate(String operate) {
		this.operate = operate;
	}

}